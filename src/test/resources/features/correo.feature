Feature: Correo electrónico en gmail.
  Como interesado en un puesto laboral
  Quiero revisar la recepción del correo electrónico en mi cuenta de gmail
  Para estar enterado de la respuesta de la entrevista de mi oferta laboral

  Scenario Outline: Ingresar a gmail.
    Given que estoy en la pagina de inicio de sesion.
    When ingreso con mi <Cuenta> y <contrasena>
    Then puedo ver los correos que me han enviado.

    Examples:
      |Cuenta                   |contrasena       |
      |"camillomurcia@gmail.com"|"Maritza-20032015"|