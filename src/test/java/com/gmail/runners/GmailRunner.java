package com.gmail.runners;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import cucumber.api.SnippetType;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features="src/test/resources/features/correo.feature",
        glue = "com.gmail.stepdefinitions",
        snippets = SnippetType.CAMELCASE
)

public class GmailRunner {
}
