package com.gmail.stepdefinitions;

import com.gmail.steps.GmailHomePage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class StepDefinitions {

    @Steps
    GmailHomePage correo;

    @Given("^que estoy en la pagina de inicio de sesion\\.$")
    public void queEstoyEnLaPaginaDeInicioDeSesion() throws Exception {
        correo.abrirPaginaGmail();
    }

    @When("^ingreso con mi \"([^\"]*)\" y \"([^\"]*)\"$")
    public void ingresoConMiY(String arg1, String arg2) throws Exception {
        correo.iniciarSesionEnGmail(arg1, arg2);
    }

    @Then("^puedo ver los correos que me han enviado\\.$")
    public void puedoVerLosCorreosQueMeHanEnviado() throws Exception {
        correo.validacionDeRecepcion();
    }

}
