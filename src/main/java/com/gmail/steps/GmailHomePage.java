package com.gmail.steps;

import com.gmail.pages.HomePage;
import net.thucydides.core.annotations.Step;

public class GmailHomePage {

    HomePage homePage;

    @Step
    public void abrirPaginaGmail(){
        homePage.open();
    }

    @Step
    public void iniciarSesionEnGmail(String correo, String contrasena){
        homePage.ingresar(correo, contrasena);
    }

    @Step
    public void validacionDeRecepcion(){
        homePage.validar();
    }
}
