package com.gmail.pages;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("https://www.gmail.com")
public class HomePage extends PageObject {

    @FindBy (id = "identifierId")
    WebElement inputCorreo;

    @FindBy (id = "identifierNext")
    WebElement botonSiguiente;

    @FindBy (name = "password")
    WebElement inputContra;

    @FindBy (id = "passwordNext")
    WebElement iniciarSesion;

    public void ingresar(String correo, String contrasena){
        inputCorreo.sendKeys(correo);
        botonSiguiente.click();
        inputContra.sendKeys(contrasena);
        iniciarSesion.click();
    }

    @FindBy (className = "UI")
    WebElement recepcionCorreos;

    public void validar(){
        recepcionCorreos.isDisplayed();
    }
}
